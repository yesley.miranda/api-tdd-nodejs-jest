module.exports = (sequelize, DataTypes) => {
  const Sistema = sequelize.define('Sistema', {
    nome: DataTypes.STRING
  });

  return Sistema;
};
