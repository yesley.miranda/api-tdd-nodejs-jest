module.exports = {
  username: "postgres",
  password: "123456",
  database: "agencia",
  host: "127.0.0.1",
  dialect: "postgres",
  operatorsAliases: 0,
  logging: 0,
  define: {
    timestamps: 1,
    underscored: 1,
    underscoredAll: 1
  }
};
