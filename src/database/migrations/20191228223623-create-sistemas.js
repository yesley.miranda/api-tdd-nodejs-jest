'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('sistemas', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: 1,
        autoIncrement: 1,
        allowNull: 0
      },
      nome: {
        type: Sequelize.STRING,
        allowNull: 0
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull: 0
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: 0
      }
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('sistemas');
  }
};
